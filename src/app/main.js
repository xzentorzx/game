import {Scene} from './classes/engine/scene'
import {Ball} from "./classes/ball";

import '../scss/app.scss'

const canvas = document.getElementById('canvas');
const fps = 30;

const scene = new Scene(fps, canvas);
const ball = new Ball(20, 30, 30, 1, 2);

if (!scene.ready)
{
    // Wait for scene
    const sceneReady = scene.readyState.subscribe('sceneReady', () => {
        scene.run();
        sceneReady(); // Unsubscribe
    });
} else
{
    scene.run();
}

function drawCircle(circle)
{
    scene.cx.beginPath();
    scene.cx.fillStyle = 'red';
    scene.cx.arc(circle.xPos, circle.yPos, circle.radius, 0, Math.PI * 360);
    scene.cx.fill();

    if (circle.xPos + circle.radius >= scene.cw || circle.xPos - circle.radius <= 0)
    {
        circle.xVel *= -1;
    }
    if (circle.yPos + circle.radius >= scene.ch || circle.yPos - circle.radius <= 0)
    {
        circle.yVel *= -1;
    }

    circle.xPos += circle.xVel;
    circle.yPos += circle.yVel;
}

scene.render = () => {
    drawCircle(ball);
};
