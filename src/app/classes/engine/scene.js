import {Emitter} from "../utils/emitter";

export class Scene {
    constructor(fps, canvas)
    {
        this.readyState = new Emitter();

        this.canvas = canvas;
        this.interval = 1000 / fps;

        this.cw = canvas.width;
        this.ch = canvas.height;

        this.lastTime = (new Date()).getTime();
        this.currentTime = 0;
        this.delta = 0;
        this.vendors = ['webkit', 'moz'];

        this.cx = canvas.getContext('2d');

        this.setAnimationFrame();
    }

    setAnimationFrame()
    {
        for (let x = 0; x < this.vendors.length && !window.requestAnimationFrame; ++x)
        {
            window.requestAnimationFrame = window[this.vendors[x] + 'RequestAnimationFrame'];
            window.cancelAnimationFrame =
                window[this.vendors[x] + 'CancelAnimationFrame'] || window[this.vendors[x] + 'CancelRequestAnimationFrame'];
        }

        if (typeof (this.canvas.getContext) !== undefined)
        {
            this.ready = true;
            console.log('sceneReady');
            this.readyState.emit('sceneReady');
        }
    }

    gameLoop()
    {
        this.animationFrame = window.requestAnimationFrame(() => {
            this.gameLoop();
        });

        this.currentTime = (new Date()).getTime();
        this.delta = (this.currentTime - this.lastTime);

        if (this.delta > this.interval)
        {
            this.resetScreen();
            this.render();
            this.lastTime = this.currentTime - (this.delta % this.interval);
        }
    }

    resetScreen()
    {
        this.cx.clearRect(0, 0, this.cw, this.ch);
    }

    render()
    {
    }

    run()
    {
        this.gameLoop();
    }

    pause()
    {
        cancelAnimationFrame(this.animationFrame);
    }
}