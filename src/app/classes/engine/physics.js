import {Emitter} from "../utils/emitter";

export class BorderCollider {
    constructor(object, border)
    {
        this.collisionDetector = new Emitter();

        this.object = object;
        this.border = border;
    }
}