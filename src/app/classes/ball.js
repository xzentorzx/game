export class Ball {
    constructor(radius, xPos, yPos, xVel, yVel)
    {
        this.radius = radius;
        this.xPos = xPos;
        this.yPos = yPos;
        this.xVel = xVel;
        this.yVel = yVel;
    }
}